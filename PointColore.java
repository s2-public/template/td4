public class PointColore extends Point {
	private Couleur couleur;
	
	public static final PointColore POINTCOLOREE = new PointColore(0.0F, 0.0F, 0 , 0, 0);
	
	public PointColore(float abscisse, float ordonnee, int rouge, int vert, int bleu) throws IllegalArgumentException {
		super(abscisse, ordonnee);
		this.couleur = new Couleur(rouge,vert,bleu);
	}
	
	public Couleur getCouleur() {
		return this.couleur;
	}
	
	@Override
	public String toString() {
		return super.toString()+" "+ this.couleur.toString();
	}
}
